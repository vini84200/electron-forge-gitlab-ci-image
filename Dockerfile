FROM node:latest
LABEL Name=electronforgegitlabciimage Version=0.0.1
RUN apt-get -y update && apt-get install -y fortunes
RUN apt-get update -y
RUN apt install -y apt-transport-https dirmngr gnupg ca-certificates
RUN apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 3FA7E0328081BFF6A14DA29AA6A19B38D3D831EF
RUN echo "deb https://download.mono-project.com/repo/debian stable-buster main" | tee /etc/apt/sources.list.d/mono-official-stable.list
RUN apt update
RUN dpkg --add-architecture i386 
RUN apt-get update
RUN apt-get install -y dpkg fakeroot rpm wine mono-devel wine32 wine64 libwine libwine:i386 fonts-wine

